///////////// GLOBAL VARIABLES /////////////////////////////////////

// Course Name, Code and Description
var COURSE_NAME = "SCORM 1.2 Demo Course";
var COURSE_CODE = "DEMO";

var DISPLAY_CERTIFICATE = false;
var DISPLAY_CERTIFICATE_FOR_COMPLETED_COURSE = false;
var DISPLAY_SCORE_ON_CERTIFICATE = false;

/////////////Set Page Title//////////////////////////////
function SetPageTitle()
{
	document.title = COURSE_NAME;
}
SetPageTitle();

///////////// END /////////////////////////////////////